require 'test_helper'

class RepliesHelperTest < ActionView::TestCase

  test 'italic text should work' do
    assert_bmark 'Dit moet in *italics*!', 'Dit moet in <i>italics</i>!'
  end

  test 'bold text should work' do
    assert_bmark 'Zeer **vet**!', 'Zeer <b>vet</b>!'
  end

  test 'inline code should work' do
    assert_bmark 'Check mijn code `tryAgain();`.', 'Check mijn code <code>tryAgain();</code>.'
  end

  test 'bold or italic test between quotes should not be changed' do
    assert_bmark 'Code: `1*1=2 2*2=4 **not bold**`.', 'Code: <code>1*1=2 2*2=4 **not bold**</code>.'
  end

  test 'linebreaks should be changed to <br> tags' do
    assert_bmark "Hello\r\nHello!", 'Hello<br>Hello!'
  end

  test 'trailing spaces should be stripped' do
    assert_bmark 'lel               kek           ', 'lel               kek'
  end

  test 'trailing linebreaks should be stripped as well' do
    assert_bmark "kekkek \r\n\r\n\r\n\r\n", 'kekkek'
  end

  test 'links should be converted to <a> tags' do
    assert_bmark 'Amai: https://eff.org?x=1&y=2.',
      'Amai: <a href="https://eff.org?x=1&amp;y=2" rel="nofollow noreferrer">https://eff.org?x=1&amp;y=2</a>.'
  end

  test 'links with brackets should also be converted correctly' do
    assert_bmark '(http://en.wikipedia.org/wiki/Sprite_(computer_graphics))',
      '(<a href="http://en.wikipedia.org/wiki/Sprite_(computer_graphics)" rel="nofollow noreferrer">http://en.wikipedia.org/wiki/Sprite_(computer_graphics)</a>)'
  end

  test 'html tags should be converted to html-safe values' do
    assert_bmark '<kek>', '&lt;kek&gt;'
  end

  test 'spoiler tags should work' do
    assert_bmark '[/s Dit is een spoiler]', '<s>Dit is een spoiler</s>'
  end

  test 'spoiler tags between code should not be converted' do
    assert_bmark '`Spoilers: [/s Iedereen gaat dood.]`',
      '<code>Spoilers: [/s Iedereen gaat dood.]</code>'
  end

  private
    def assert_bmark(bmark_formatted_text, expected_html)
      assert_equal bmark(bmark_formatted_text), expected_html
    end

end
