require 'test_helper'

# Keep all logic related to dicerolls in here.

class DicerollsTest < ActiveSupport::TestCase

  setup do
    @topic = topics(:one)
    @regex = '\{d\![^\{]+![^\{]+![^\{]+\}'
  end

  test 'shouldn\'t be able to use the dice format in messages' do
    reply = Reply.new(body: "blabla ik rol: {d!1d6!6!1d6 = 6}!", topic: @topic)
    assert_not reply.valid?
    assert reply.errors[:body].grep /bevat ongeldige tekst/
  end

  test 'a normal dice roll should be valid' do
    reply = Reply.new(body: "Ik gooi: #1d6 #d20\r\n", topic: @topic)
    assert reply.valid?
    assert reply.save
    assert_match /\AIk gooi: #{@regex} #{@regex}\r\n\z/, reply.body
  end

  test 'more than 100 sides shouldn\'t be allowed' do
    reply = Reply.new(topic: @topic)
    ['#1d101', '#6d200', '#d4000'].each do |v|
      reply.body = v
      assert_not reply.valid?
      assert reply.errors[:body].grep /max zijden/
    end
  end

  test 'more than 10 dice in one roll shouldn\'t be allowed' do
    reply = Reply.new(topic: @topic)
    ['#11d6', '#20000d4', '#100d2'].each do |v|
      reply.body = v
      assert_not reply.valid?
      assert reply.errors[:body].grep /max teerlingen in één/
    end
  end

  test 'modifiers should be within range' do
    reply = Reply.new(topic: @topic)
    ['#d6+100', '#2d20+1000', '#10d100-9999'].each do |v|
      reply.body = v
      assert_not reply.valid?
      assert reply.errors[:body].grep /modifiers zijn min/
    end
  end

  test 'shouldn\'t be able to put more than 5 dice rolls in one reply' do
    reply = Reply.new(body: '#1d6 '*6, topic: @topic)
    assert_not reply.valid?
    assert reply.errors[:body].grep /teveel teerlingworpen/
  end

end
