require 'test_helper'

class TopicTest < ActiveSupport::TestCase

  test 'topic without required fields should not be valid' do
    topic = Topic.new
    assert_not topic.valid?
    assert_equal [:body, :room, :subject], topic.errors.keys
  end

  test 'should not be able to create in non-existing room' do
    topic = Topic.new(subject: 'x', body: 'x', room_id: 100000)
    assert_not topic.save
  end

  test 'shouldn\'t be able to use url\'s in subjects' do
    room = rooms(:one)
    topic = Topic.new(subject: 'Visit my site http://coolshop.com?refer=1 !!!', body: 'x', room: room)
    assert_not topic.valid?
    assert topic.errors[:subject].grep /spam/
  end

  test 'emojis shouldn\'t be allowed in subject' do
    room = rooms(:one)
    topic = Topic.new(body: 'x', room: room)
    topic.subject = '😄'
    assert_not topic.valid?
    assert topic.errors[:subject].grep /emoji/
    topic.subject = '🌈'
    assert_not topic.valid?
    assert topic.errors[:subject].grep /emoji/
  end

  test 'asian chars should be allowed in subject' do
    room = rooms(:one)
    topic = Topic.new(body: 'x', room: room)
    topic.subject = 'ゲームセンター'
    assert topic.valid?
  end

end
