require 'test_helper'

class ReplyTest < ActiveSupport::TestCase

  test 'creating a reply on an existing topic should work' do
    topic = topics(:one)
    reply = Reply.new({body: 'x', topic: topic})
    assert reply.valid?
    assert reply.save
  end

  test 'reply without required fields should not be valid' do
    reply = Reply.new
    assert_not reply.valid?
    assert_equal [:body, :topic], reply.errors.keys
  end

  test 'creating without name should set author to room default' do
    topic = topics(:two)
    reply = Reply.create!({body: 'x', topic: topic})
    assert_equal reply.author, topic.room.default_author
  end

  test 'when specifying a name, it should be set when saving' do
    topic = topics(:one)
    name = 'Tester-san'
    reply = Reply.create!({name: name, body: 'x', topic: topic})
    assert_equal reply.author, name
  end

  test 'names longer than 20 chars should be truncated' do
    topic = topics(:one)
    name = '123456789abcdefghijklmnopqrstuvwxyz'
    reply = Reply.create!({name: name, body: 'x', topic: topic})
    assert_equal 20, reply.author.length
  end

  test 'should not be able to have empty name using special input' do
    topic = topics(:one)
    name = '###########'
    reply = Reply.create!({name: name, body: 'x', topic: topic})
    assert_equal reply.author, topic.room.default_author
  end

  test 'shouldn\'t be able to add another reply after max_replies has been reached' do
    topic = topics(:two)
    assert_equal topic.replies.size, 0
    5.times { topic.replies.create!({body: 'x'}) }
    assert_equal topic.replies.size, topic.room.max_replies
    reply = Reply.new({body: 'x', topic: topic})
    assert_not reply.valid?
    assert_not reply.save
    assert reply.errors[:base].grep /geen berichten meer toegevoegd worden/
  end

  test 'on room with max_replies 0 we should be able to add as many replies as we want' do
    topic = topics(:one)
    assert_equal topic.replies.size, 0
    assert 200.times { topic.replies.create!({body: 'x'}) }
  end

  test 'reply body should be able to contain emoji' do
    topic = topics(:two)
    reply = Reply.new({body: '🐶', topic: topic})
    assert reply.valid?
  end

  test 'reply author should be able to contain emoji' do
    topic = topics(:two)
    reply = Reply.new({author: '🐶', body: 'x', topic: topic})
    assert reply.valid?
  end

  test 'reply body should be able to contain asian chars' do
    topic = topics(:two)
    reply = Reply.new({body: 'ム', topic: topic})
    assert reply.valid?
  end

  test 'reply author should be able to contain asian chars' do
    topic = topics(:two)
    reply = Reply.new({author: 'ム', body: 'x', topic: topic})
    assert reply.valid?
  end


end
