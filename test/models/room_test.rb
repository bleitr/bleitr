require 'test_helper'

class RoomTest < ActiveSupport::TestCase

  test 'creating a room should work' do
    room = Room.new({name: 'nieuw'})
    assert room.valid?
    assert room.save
  end

  test 'name should be required when creating room' do
    room = Room.new
    assert_not room.valid?
    assert_equal [:name], room.errors.keys
  end

  test 'should not be able to create a room with an existing name' do
    room = Room.new({name: rooms(:one).name})
    assert_not room.valid?
  end

end
