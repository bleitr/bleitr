require 'test_helper'

class RoutingTest < ActionDispatch::IntegrationTest

  test 'routes should map to correct controllers and actions' do
    assert_routing '/', controller: 'home', action: 'index'
    assert_routing '/b', controller: 'rooms', action: 'index'
    assert_routing '/b/kek', controller: 'rooms', action: 'show', room_name: 'kek'
    assert_routing '/b/kek/23', controller: 'topics', action: 'show', room_name: 'kek', topic_id: '23'
  end

  test 'ajax routes should map as well' do
    assert_routing '/js/preview/1/2', controller: 'replies', action: 'hover', topic_id: '1', reply_fakeid: '2'
  end

end




