require 'test_helper'

class RepliesControllerTest < ActionDispatch::IntegrationTest

  setup do
    @params = {
      reply: {
        name: '',
        email: '',
        options: '',
        body: 'Dit is een test reactie van in Rails. **Hallo!**',
      }
    }
  end

  test 'should be able to see replies' do
    topic = topics :one
    get topic_url topic.room.name, topic.id
    assert_response :success
    topic.replies.each do |r|
      assert_select "div##{r.fakeid}", {count: 1}
    end
  end

  test 'should be able to see hover replies via ajax' do
    topic = topics(:one)
    reply = topic.replies.last
    get (js_preview_url topic.id, reply.fakeid), xhr: true
    assert_response :success
    assert_match 'function()', @response.body  # Make sure we get JS back.
    assert_match reply.fakeid.to_s, @response.body
  end

  test 'should be able to create a new reply' do
    topic = topics(:one)
    assert_difference('Reply.count', +1) do
      post topic_url(topic.room.name, topic.id), params: @params
    end
    assert_response :found
    assert_redirected_to topic_url(topic.room.name, topic.id, p: topic.last_page, anchor: 'l')
  end

  test 'should be able to create a new reply via ajax' do
    topic = topics(:one)
    assert_difference('Reply.count', +1) do
      post topic_url(topic.room.name, topic.id), params: @params, xhr: true
    end
    assert_response :success
    assert_match 'function()', @response.body
  end

  test 'reply via ajax with invalid text should give 422 status code' do
    topic = topics(:one)
    @params[:reply][:body] = '#1337d1337'
    post topic_url(topic.room.name, topic.id), params: @params, xhr: true
    assert_response :unprocessable_entity
    assert_match 'function()', @response.body
    assert_match 'message is-danger', @response.body
  end

  test 'using nonoko should redirect back to room index' do
    topic = topics(:two)
    @params[:reply][:options] = 'nonoko'
    assert_difference('Reply.count', +1) do
      post topic_url(topic.room.name, topic.id), params: @params
    end
    assert_response :found
    assert_redirected_to room_url(topic.room.name)
  end

  test 'should not be able to create a reply without body' do
    topic = topics(:one)
    @params[:reply][:body] = ''
    assert_difference('Reply.count', 0) do
      post topic_url(topic.room.name, topic.id), params: @params
    end
  end

  test 'spam protection should be triggered when :email field is posted' do
    topic = topics(:one)
    @params[:reply][:email] = 'john@example.com'
    assert_difference('Reply.count', 0) do
      post topic_url(topic.room.name, topic.id), params: @params
    end
    assert_response :found
    assert @response.body, ''
  end

  test 'visiting ajax preview path without xhr should give 404' do
    topic = topics(:one)
    reply = topic.replies.last
    assert_raises(ActionController::RoutingError) do
      get js_preview_url(topic, reply)
      assert_response :not_found
    end
  end

end
