require 'test_helper'

class TopicsViewTest < ActionDispatch::IntegrationTest

  test 'button should be enabled on topic view' do
    topic = topics :one
    get topic_url topic.room.name, topic.id
    assert_select 'a.button.is-primary', {count: 1, text: 'Nieuw Bericht'} do
      assert_select ":match('disabled', 'disabled')", false
    end
  end

  test 'should be able to see topic subject' do
    topic = topics :one
    get topic_url topic.room.name, topic.id
    assert_select 'span.tag', {count: 1, text: topic.subject}
  end

end
