require 'test_helper'

class RoomsControllerTest < ActionDispatch::IntegrationTest

  test 'should get a list of rooms' do
    get rooms_url
    assert_response :success
    assert_match rooms(:one).name, @response.body
    assert_match rooms(:two).description, @response.body
  end

  test 'should give 404 when going to non-existing room' do
    assert_raises(ActionController::RoutingError) do
      get room_url('blabla')
      assert_response :not_found
    end
  end

  test 'should be able to get index for an existing room via its name' do
    get room_url(rooms(:one).name)
    assert_response :success
  end

  test 'specifying the room id as param should not work' do
    assert_raises(ActionController::RoutingError) do
      get room_url(rooms(:one).id)
      assert_response :not_found
    end
  end

end
