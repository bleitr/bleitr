require 'test_helper'

class TopicsControllerTest < ActionDispatch::IntegrationTest

  setup do
    @params = {
      topic: {
        name: '',
        email: '',
        options: '',
        subject: 'Hallo ik ben aan het testen!',
        body: 'Dit is een test bericht van in Rails. **Hallo!**',
      }
    }
  end

  test 'should be able to visit topic' do
    topic = topics(:one)
    get topic_url(topic.room.name, topic.id)
    assert_response :success
  end

  test 'should give 404 when going to non-existing topic' do
    room = rooms(:one)
    assert_raises(ActionController::RoutingError) do
      get topic_url(room.name, 1234567)
      assert_response :not_found
    end
  end

  test 'should be able to create a new topic' do
    room = rooms(:one)
    assert_difference('Topic.count', +1) do
      post room_url(room.name), params: @params
    end
    assert_response :found
    assert_redirected_to topic_url(room.name, Topic.last)
    assert_equal Topic.last.replies.size, 1
  end

  test 'using nonoko should redirect back to room index' do
    room = rooms(:two)
    @params[:topic][:options] = 'nonoko'
    assert_difference('Topic.count', +1) do
      post room_url(room.name), params: @params
    end
    assert_response :found
    assert_redirected_to room_url(room.name)
  end

  test 'should not be able to create a topic without subject' do
    room = rooms(:one)
    @params[:topic][:subject] = ''
    assert_difference('Topic.count', 0) do
      post room_url(room.name), params: @params
    end
  end

  test 'should not be able to create a topic without body' do
    room = rooms(:one)
    @params[:topic][:body] = ''
    assert_difference('Topic.count', 0) do
      post room_url(room.name), params: @params
    end
  end

  test 'spam protection should be triggered when :email field is posted' do
    room = rooms(:one)
    @params[:topic][:email] = 'john@example.com'
    assert_difference('Topic.count', 0) do
      post room_url(room.name), params: @params
    end
    assert_response :found
    assert @response.body, ''
  end

end
