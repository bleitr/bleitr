require 'test_helper'

class RoomsViewTest < ActionDispatch::IntegrationTest

  test 'new button should be disabled on list of rooms' do
    get rooms_url
    assert_select 'a.button.is-primary', {count: 1, text: 'Nieuw Bericht'} do
      assert_select ":match('disabled', 'disabled')"
    end
  end

  test 'new button should be enabled on room page' do
    get room_url(rooms(:one).name)
    assert_select 'a.button.is-primary', {count: 1, text: 'Nieuw Bericht'} do
      assert_select ":match('disabled', 'disabled')", false
    end
  end

end
