require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest

  test 'should view topics on homepage' do
    get root_url
    assert_response :success
    assert_match topics(:one).subject, @response.body
  end

  test 'should view topics via ajax' do
    get root_url, xhr: true
    assert_response :success
    assert_match 'function()', @response.body  # Just to make sure we're getting JS back.
    assert_match topics(:one).subject, @response.body
  end

end
