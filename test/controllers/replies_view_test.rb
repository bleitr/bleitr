require 'test_helper'

class RepliesViewTest < ActionDispatch::IntegrationTest

  test 'first reply should have EB value' do
    reply = replies(:one)
    assert reply.is_op
    get topic_url reply.topic.room.name, reply.topic.id
    assert_select "div##{reply.fakeid}>.media>.media-content>.content>abbr", {count: 1, text: 'EB'}
  end

  test 'second reply shouldn\'t have EB value' do
    reply = replies(:three)
    assert_not reply.is_op
    get topic_url reply.topic.room.name, reply.topic.id
    assert_select "div##{reply.fakeid}>.media>.media-content>.content>abbr", {count: 0}
  end

end
