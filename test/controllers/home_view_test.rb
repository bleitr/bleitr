require 'test_helper'

class HomeViewTest < ActionDispatch::IntegrationTest

  test 'title should be set' do
    get root_url
    assert_select 'title', Rails.configuration.x.title
  end

  test 'robots file should disallow indexing everywhere' do
    get "#{root_url}/robots.txt"
    assert_match /User-agent: \*\RDisallow: \//, @response.body
  end

  test 'new button should be disabled on homepage' do
    get root_url
    assert_select "a.button.is-primary:match('disabled', 'disabled')",
      {count: 1, text: 'Nieuw Bericht'}
  end

end
