rooms = [
  {
    name: 'anime',
    description: 'Alles over anime & manga.',
    rules: 'Dit is /b/anime. Waar 2D altijd wint van 3D.',
    default_author: '名前',
    sort_order: -50,
  },
  {
    name: 'tv',
    description: 'TV en film.',
    rules: 'Welkom in /b/tv.',
    sort_order: -60,
  },
  # {
  #   name: 'vertel10',
  #   description: 'Tien berichten om een verhaal te vertellen.',
  #   rules: 'Tien berichten om een verhaal te vertellen.',
  #   max_replies: 10,
  # },
  {
    name: 'meta',
    description: 'Waar de échte bleiters uithangen.',
    rules: 'De enige en onvervalste /b/meta, uw wankele houvast naar de werkelijkheid.',
    purge_delay: 10080,
    sort_order: 100,
  },
  # {
  #   name: 'annonces',
  #   description: 'Fake zoekertjes.',
  #   rules: '/b/annonces: Enkel oprechte en discrete zoekertjes. Beloofd.',
  # },
  # {
  #   name: 'erobotiek',
  #   description: 'Cyborgs en seks.',
  #   rules: 'Dompel je onder in de noir wereld van /b/erobotiek, een cocktail van
  #           spandex en printplaten in extase.',
  # },
  {
    name: 'rp',
    description: 'Role-playing games en RP-klap.',
    rules: 'Hallo avonturier! Welkom op <b>/b/rp</b>.
            <p>
            Hier kan je online campaigns runnen, free-form of met een systeem
            naar jouw keuze. Algemene RP-klap is hier ook toegelaten.
            </p>',
    sort_order: 50,
    purge_delay: 129600, # 90d
  },
  {
    name: 'tech',
    description: 'Technologie dingen. Niet enkel voor robots.',
    rules: '01100010 00101111 01110100 01100101 01100011 01101000',
    sort_order: -40,
  },
  {
    name: 'logs',
    description: 'Kaklog en co.',
  },
  # {
  #   name: 'sekspiraat',
  #   description: 'Seks op de hoge zeeën.',
  #   rules: 'ARRRRR.',
  # },
]

puts 'Creating rooms...'
Room.create(rooms)
