class CreateTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :topics do |t|
      t.string :author, null: false
      t.string :tripcode
      t.string :subject, null: false
      t.text :body, null: false
      t.datetime :bump_date, null: false
      t.references :room, foreign_key: true
      t.integer :options

      t.timestamps
    end
  end
end
