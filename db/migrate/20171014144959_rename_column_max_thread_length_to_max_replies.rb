class RenameColumnMaxThreadLengthToMaxReplies < ActiveRecord::Migration[5.1]
  def change
    rename_column(:rooms, :max_thread_length, :max_replies)
  end
end
