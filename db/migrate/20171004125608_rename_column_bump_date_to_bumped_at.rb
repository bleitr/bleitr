class RenameColumnBumpDateToBumpedAt < ActiveRecord::Migration[5.1]
  def change
    rename_column(:topics, :bump_date, :bumped_at)
  end
end
