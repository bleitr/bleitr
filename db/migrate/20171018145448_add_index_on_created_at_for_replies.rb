class AddIndexOnCreatedAtForReplies < ActiveRecord::Migration[5.1]
  def change
    add_index :replies, :created_at
  end
end
