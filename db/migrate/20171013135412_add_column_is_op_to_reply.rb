class AddColumnIsOpToReply < ActiveRecord::Migration[5.1]
  def change
    add_column :replies, :is_op, :boolean, default: false
  end
end
