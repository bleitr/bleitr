class AddIndexesAgainWithSortOrder < ActiveRecord::Migration[5.1]
  def change
    remove_index :topics, :bumped_at
    add_index :topics, :bumped_at, order: { bumped_at: :desc }
    add_index :rooms, :order, order: { order: :desc }
  end
end
