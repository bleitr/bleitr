class RemoveSomeColumnsFromTopic < ActiveRecord::Migration[5.1]
  def change
    remove_column :topics, :author
    remove_column :topics, :tripcode
    remove_column :topics, :body
    remove_column :topics, :options
  end
end
