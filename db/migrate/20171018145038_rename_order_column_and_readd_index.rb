class RenameOrderColumnAndReaddIndex < ActiveRecord::Migration[5.1]
  def change
    remove_index :rooms, :order
    rename_column :rooms, :order, :sort_order
    add_index :rooms, :sort_order, order: { sort_order: :desc }
  end
end
