class AddDefaultAuthorToRoom < ActiveRecord::Migration[5.1]
  def change
    add_column :rooms, :default_author, :string, default: 'Anoniem'
  end
end
