class AddSomeIndexOnColumns < ActiveRecord::Migration[5.1]
  def change
    add_index :topics, :bumped_at, order: { bumped_at: :desc }
    remove_index :rooms, :order
    add_index :rooms, :order, order: { order: :desc }
  end
end
