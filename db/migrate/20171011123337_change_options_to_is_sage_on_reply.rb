class ChangeOptionsToIsSageOnReply < ActiveRecord::Migration[5.1]
  def change
    remove_column :replies, :options
    add_column :replies, :is_sage, :boolean, default: false
  end
end
