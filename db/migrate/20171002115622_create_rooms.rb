class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.string :name, null: false
      t.string :description
      t.text :rules
      t.integer :purge_delay, default: 43200
      t.integer :max_thread_length, default: 0
      t.integer :order, default: 0

      t.timestamps
    end
    add_index :rooms, :name, unique: true
    add_index :rooms, :order, order: :desc
  end
end
