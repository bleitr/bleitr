class CreateReplies < ActiveRecord::Migration[5.1]
  def change
    create_table :replies do |t|
      t.string :author, null: false
      t.string :tripcode
      t.text :body, null: false
      t.references :topic, foreign_key: true
      t.integer :options

      t.timestamps
    end
  end
end
