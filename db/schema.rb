# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171018145448) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "replies", force: :cascade do |t|
    t.string "author", null: false
    t.string "tripcode"
    t.text "body", null: false
    t.integer "topic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_sage", default: false
    t.boolean "is_op", default: false
    t.index ["created_at"], name: "index_replies_on_created_at"
    t.index ["topic_id"], name: "index_replies_on_topic_id"
  end

  create_table "rooms", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.text "rules"
    t.integer "purge_delay", default: 43200
    t.integer "max_replies", default: 0
    t.integer "sort_order", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "default_author", default: "Anoniem"
    t.index ["name"], name: "index_rooms_on_name", unique: true
    t.index ["sort_order"], name: "index_rooms_on_sort_order", order: { sort_order: :desc }
  end

  create_table "topics", force: :cascade do |t|
    t.string "subject", null: false
    t.datetime "bumped_at", null: false
    t.integer "room_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "replies_count", default: 0
    t.index ["bumped_at"], name: "index_topics_on_bumped_at", order: { bumped_at: :desc }
    t.index ["room_id"], name: "index_topics_on_room_id"
  end

end
