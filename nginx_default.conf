upstream app {
    server rails:3000;
    keepalive 8;
}

server {

    listen 80 default_server;
    server_name bleitr.rbox.be;

    root /var/lib/nginx/html/public;

    # Compression.
    gzip on;
    gzip_min_length 1024;
    gzip_types
        application/javascript
        application/octet-stream
        text/css
        text/javascript
        text/plain;

    try_files $uri @app;

    location @app {
        proxy_pass http://app;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Ssl on;
        proxy_set_header X-Forwarded-Port $server_port;
        proxy_set_header X-Forwarded-Host $host;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

    location ~ ^/assets/ {
        expires 1y;
        add_header Cache-Control public;
        add_header ETag "";
    }

    error_page 404 /404.html;
    error_page 422 /422.html;
    error_page 500 502 503 504 /500.html;

}
