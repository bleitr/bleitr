desc 'This deletes old topics and their replies, relying on bumped_at and purge_delay.'
task :purger => :environment do
  for room in Room.all
    purge_before = DateTime.now - room.purge_delay.minutes
    #purge_before = DateTime.now - 10.seconds
    id_list = Topic.select(:id).where(room: room).where("bumped_at < ?", purge_before).map { |r| r.id }
    if not id_list.empty?
      ActiveRecord::Base.transaction do
        Topic.where(id: id_list).delete_all
        Reply.where(topic_id: id_list).delete_all
      end
    end
  end
end
