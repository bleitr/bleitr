desc 'This moves a topic to another room.'
task :move_topic, [:topic_id, :room] => :environment do |t, args|
  # Find room
  dest_room = Room.find_by_name args[:room]
  if dest_room
    # Find topic
    topic = Topic.find args[:topic_id]
    if topic
      topic.body = 'dummy'
      topic.room = dest_room
      topic.save!
      puts "Moved topic \"#{topic.subject}\" to room \"#{dest_room.name}\"."
    end
  end
end
