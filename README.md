# bleitr

Een tekstbord programma in Rails, met vrije broncode.

## Configuratie

* Opties in `config/initializers/bleitr_config.rb`
* Database settings in `config/database.yml`
* Redis settings in `config/cable.yml`

## Licentie

[MIT Licentie](https://opensource.org/licenses/MIT)

Copyright 2017-2019 Mullepaté.
