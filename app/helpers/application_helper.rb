module ApplicationHelper
  include IconHelper

  def button_new
    return disabled_button if controller.action_name != 'show'

    case controller.controller_name
    when 'rooms' then return active_button
    when 'topics'
      return topic_has_reached_max_replies? ? disabled_button : active_button
    end
    disabled_button
  end

  def rooms_nav
    case controller.controller_name
    when 'home' then make_nav_link('alles', rooms_path, 'asterisk')
    when 'rooms'
      make_nav_link(@room.name, rooms_path) if controller.action_name == 'show'
    when 'topics' then make_nav_link(@room.name, room_path(@room))
    else ''
    end
  end

  private

  def make_nav_link(name, path, icon = 'circle')
    link_to icon(icon) + name, path, class: 'navbar-item is-room-nav'.freeze
  end

  def active_button
    link_to 'Nieuw Bericht', '#f', class: 'button is-primary is-main',
                                   title: 'alt+n'
  end

  def disabled_button
    content_tag 'a', 'Nieuw Bericht', class: 'button is-primary', disabled: true
  end
end
