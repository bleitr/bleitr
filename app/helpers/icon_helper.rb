module IconHelper
  def icon(icon_name)
    content_tag 'span', content_tag(:i, '', class: "fa fa-#{icon_name}"),
                class: 'icon'
  end
end
