module StaticHelper
  def link_to_noref(name, link)
    link_to name, link, rel: 'nofollow noreferrer'
  end
end
