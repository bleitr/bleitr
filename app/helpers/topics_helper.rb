module TopicsHelper
  def topic_has_reached_max_replies?
    @room.max_replies.zero? ? false : @topic.replies.size >= @room.max_replies
  end
end
