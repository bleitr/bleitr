module RepliesHelper
  include IconHelper

  BRACKETS = {
    ']' => '[',
    ')' => '(',
    '}' => '{'
  }.freeze

  # Crude custom markup language.
  # We call it bmark code.
  def bmark(text)
    # First make the user input HTML safe.
    output = ERB::Util.html_escape text.strip

    # Spoiler tags.
    output.gsub!(%r{\[/s ([^\]\n]+)\](?![^\n`]*`[^\n`]*$)}, '<s>\1</s>')

    # Bold text.
    output.gsub!(/\*\*([^\*\n]+)\*\*(?![^\n`]*`[^\n`]*$)/, '<b>\1</b>')

    # Emphasized text.
    output.gsub!(/\*([^\*\n]+)\*(?![^\n`]*`[^\n`]*$)/, '<i>\1</i>')

    # Strikethrough text.
    output.gsub!(/\~\~([^\~\n]+)\~\~(?![^\n`]*`[^\n`]*$)/,
                 '<strike>\1</strike>')

    # Inline code.
    # The lookahead (?![^\n`]*`[^\n`]*$) in previous lines is used not to match
    # anything between code tags (backticks).
    output.gsub!(/`([^`\n]+)`/, '<code>\1</code>')

    # Auto-link.
    output = output.gsub(%r{https?://[^\s\<]+}) do |s|
      # Don't include trailing punctuation character as part of the URL.
      # Code from https://github.com/tenderlove/rails_autolink/blob/master/lib/rails_autolink/helpers.rb
      punctuation = []
      while s.sub!(%r{[^\p{Word}/-=&]$}, '')
        punctuation.push $&
        opening = BRACKETS[punctuation.last] || next

        if s.scan(opening).size > s.scan(punctuation.last).size
          s << punctuation.pop
          break
        end
      end

      "<a href=\"#{s}\" rel=\"nofollow noreferrer\">#{s}</a>" \
      "#{punctuation.reverse.join('')}"
    end

    # Quotes.
    output.gsub!(/&gt;&gt;?([0-9]{5,})/,
                 '<a href="#\\1" data-preview-reply-id="\\1">&gt;&gt;\\1</a>')
    output.gsub!(/(\A|\R)(&gt;(?!&gt;)[^\n\r]+)/, '<q>\\1\\2</q>')

    # Dicerolls.
    output.gsub!(/\{d\!([^\{]+)!([^\{]+)!([^\{]+)\}/,
                 "( #{icon 'cube'}<span class=\"dice\">\\1</span>: " \
                 '<abbr title="\\3">\\2</abbr> )')

    # Linebreaks.
    output = output.gsub(/\R{3,}/, '<br><br>').gsub(/\R/, '<br>')

    # Return the result. We guarantee that it's HTML safe and can be displayed
    # as-is in the views.
    output.html_safe
  end

  def tripcode_with_prefix(tripcode)
    tripcode.length > 10 ? "!!#{tripcode}" : "!#{tripcode}"
  end
end
