$('#topics').html "<%= j render partial: 'topic', collection: @topics, cached: true %>"
$('#paginate').html "<%= j paginate @topics, params: {_: nil} %>"
