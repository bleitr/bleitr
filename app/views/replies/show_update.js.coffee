<% if @reply.options.include? 'nonoko' %>
window.location.href = "<%= room_path(@room) %>"
return
<% end %>

<% if topic_has_reached_max_replies? %>
$('#replies-ui').html "<%= j render partial: 'topics/max_reply_notice' %>"
$('a.button.is-primary').attr 'disabled', 'disabled'
<% end %>
