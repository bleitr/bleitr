class RepliesController < ApplicationController
  before_action :honey_pot, only: [:create]
  before_action :set_room, only: [:create]
  before_action :set_topic, only: [:create]
  before_action :set_id, only: [:hover]
  before_action :set_reply, only: [:hover]

  def create
    @reply = Reply.new(reply_params)
    @reply.topic = @topic
    respond_to do |format|
      format.html { @reply.save ? redirect_to(redirect_path) : render('new') }
      format.js { @reply.save ? render('show_update') : render_js_errors }
    end
  end

  def new; end

  def hover
    respond_to do |format|
      format.html { not_found }
      format.js
    end
  end

  private

  def set_room
    @room = Room.find_by(name: params[:room_name]) || not_found
  end

  def set_topic
    @topic = Topic.where(id: params[:topic_id], room: @room).take || not_found
  end

  def set_id
    return not_found unless number? params[:reply_fakeid]

    id = Integer(params[:reply_fakeid]) - Reply.new.fake_offset
    return not_found unless id.positive?

    @id = id
  end

  def set_reply
    @reply = Reply.where(id: @id, topic_id: params[:topic_id]).take || not_found
  end

  def honey_pot
    head(:found) if params[:reply][:email].present?
  end

  def number?(value)
    Integer(value)
  rescue ArgumentError
    false
  end

  def redirect_path
    if @reply.options.include? 'nonoko'
      room_path(@room)
    else
      topic_path(@room, @topic, p: @topic.last_page, anchor: 'l')
    end
  end

  def render_js_errors
    render('show_errors', status: :unprocessable_entity)
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def reply_params
    params.require(:reply).permit(:name, :options, :body)
  end
end
