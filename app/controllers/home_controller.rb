class HomeController < ApplicationController
  def index
    @topics = Topic.order(bumped_at: :desc).page(params[:p]).includes(:room)
    respond_to do |format|
      format.html
      format.js
    end
  end
end
