class TopicsController < ApplicationController
  before_action :honey_pot, only: [:create]
  before_action :set_room, only: %i[show create]
  before_action :set_topic, only: [:show]

  def show
    @reply = Reply.new
    @replies = Reply.where(topic: @topic).order(created_at: :asc)
                    .page(params[:p])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @topic = Topic.new(topic_params)
    @topic.room = @room
    return render('new') unless @topic.valid?

    # Create the topic and the first reply as one transaction.
    # If one fails, the other will not be commited. In case of bugs this
    # at least avoids we pollute the database with topics that have 0 replies.
    # Reference article: https://archive.fo/TeacK
    Topic.transaction do
      @topic.save!
      @topic.replies.create!(name: @topic.name, body: @topic.body, is_op: true,
                             options: '')
    end
    redirect_to redirect_path
  end

  def new; end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_room
    @room = Room.find_by(name: params[:room_name])
  end

  def set_topic
    @topic = Topic.where(id: params[:topic_id], room: @room).take || not_found
  end

  def honey_pot
    head(:found) if params[:topic][:email].present?
  end

  def redirect_path
    return topic_path(@room, @topic) unless @topic.options.include?('nonoko')

    room_path(@room)
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def topic_params
    params.require(:topic).permit(:name, :options, :subject, :body)
  end
end
