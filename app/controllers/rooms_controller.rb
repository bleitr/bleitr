class RoomsController < ApplicationController
  before_action :set_room, only: [:show]

  def index
    @rooms = Room.order(sort_order: :desc, name: :asc)
  end

  def show
    @topic = Topic.new
    @topics = Topic.where(room: @room).order(bumped_at: :desc).page(params[:p])
    respond_to do |format|
      format.html
      format.js
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_room
    @room = Room.find_by(name: params[:room_name]) || not_found
  end
end
