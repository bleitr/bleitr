class ReplyChannel < ApplicationCable::Channel

  def subscribed
    room = params[:room]
    stream_from room if /\A(home|(room|topic):[0-9]+)\z/.match? room
  end

end
