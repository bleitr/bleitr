class ReplyRelayJob < ApplicationJob

  def perform(reply)
    # Real-time updates in topic.
    ActionCable.server.broadcast "topic:#{reply.topic_id}",
      reply: RepliesController.render(partial: 'replies/reply', object: reply),
      reply_id: reply.fakeid

    if not reply.is_sage
      # Notifications in the associated room.
      ActionCable.server.broadcast "room:#{reply.topic.room_id}", data: ''
      # Notifications for homepage.
      ActionCable.server.broadcast 'home', data: ''
    end
  end

end
