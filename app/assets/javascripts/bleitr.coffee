# Here we load all the classes after the page is loaded.

$ ->
  new Notification
  new Navigation
  new KeyboardNavigation
  new QuotePreview
  new Pagination
  new RemoteReply
  new Spoilers
  new RemainingChars
