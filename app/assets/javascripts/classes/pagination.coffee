# Kaminari Ajax pagination.
# http://jameshuynh.com/2016/01/28/ajax-pagination-on-rails-and-kaminari/

class @Pagination
  constructor: ->
    @handleClicks()

  handleClicks: ->
    $(document).on 'click', 'nav.pagination a', (e) =>
      e.preventDefault()
      url = $(e.target).attr('href')
      @jumpToPage url, true

    # Handle back/forward buttons.
    $(window).on 'popstate', (e) =>
      $.getScript(location.href).done =>
        @postLoad()

  jumpToPage: (url, scroll) ->
    $.getScript(url).done =>
      # Scroll to top of page.
      $('body')[0].scrollIntoView() unless !scroll
      # Push URL onto history stack.
      window.history.pushState url, window.title, url
      @postLoad()

  postLoad: ->
    # Hide any hidden elements.
    $('.is-hidden-by-js').hide()
    $('#hover').hide()
