# This allows users to use keyboard shortcuts.
# E.g. alt+n, alt+r etc.
#
#= require classes/navigation

class @KeyboardNavigation extends Navigation
  constructor: ->
    @setupKeybinds()

  setupKeybinds: ->
    $(document).keydown (e) =>
      if e.altKey
        caught = true
        switch e.which
          # (n)ew message
          when 78 then @showForm()
          # quick submit (enter)
          when 13 then @submitForm()
          # (r)oot url
          when 82 then @navRoot()
          # (k)amers
          when 75 then @navRoom()
          else caught = false
      if e.ctrlKey
        caught = true
        switch e.which
          # alternative quick submit
          when 13 then @submitForm()
          else caught = false
      if caught
        e.stopImmediatePropagation()
        e.preventDefault()
