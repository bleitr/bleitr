# Simple spoiler tags. They are hoverable, but this also makes them
# clickable.

class @Spoilers
  constructor: ->
    @clickableSpoilerTags()

  clickableSpoilerTags: ->
    $(document).on 'click', 's', (e) ->
      e.preventDefault()
      $(e.target).toggleClass 'spoil_on'
