# This deals with clickable quote id's and previews when you hover
# over quoted id's.
#
#= require classes/navigation

class @QuotePreview extends Navigation
  constructor: ->
    @topic_id = $('#hover').data('topic-id')
    if @topic_id != undefined
      @clickablePostIds()
      @hoverableLinks()

  clickablePostIds: ->
    $(document).on 'click', 'a[data-reply-id]', (e) =>
      e.preventDefault()
      output = "#{$('textarea').val()}>>#{$(e.target).data 'reply-id'} "
      $('textarea').val(output) unless output.length > 1000
      @showForm()

  hoverableLinks: ->
    timeout = null
    selector = 'a[data-preview-reply-id]'
    $(document).on 'mouseover', selector, (e) =>
      timeout = setTimeout (=> @showPreview(e.target)), 300
    $(document).on 'mouseout', selector, (e) ->
      clearTimeout timeout
      $('#hover').hide()
    $(document).on 'mousemove', selector, (e) ->
      $('#hover').css(left: "#{e.clientX + 5}px", top: "#{e.clientY + 5}px")
    $(document).on 'click', selector, (e) ->
      e.preventDefault()

  showPreview: (obj) =>
    reply_id = $(obj).data 'preview-reply-id'
    inline_div = $("##{reply_id}")
    if inline_div.length
      # We get the preview from the DOM itself, saving a round-trip.
      $('#hover').html(inline_div.clone()).css(display: 'inline-block')
    else
      # It's on another page or invalid, so we launch the ajax call.
      $.getScript("/js/preview/#{@topic_id}/#{reply_id}").done ->
        $('#hover').css(display: 'inline-block')
