# This enables remote form submits. Notice that we don't show the
# reply. Displaying the reply depends on Websocket notifications.
#
# The downside is that when a user doesn't have Websockets working
# (e.g. corporate firewall), he won't see his own submitted reply.
# So this could be improved at some point.
#
#= require classes/pagination

class @RemoteReply extends Pagination
  constructor: ->
    @listenSubmit()

  listenSubmit: ->
    $(document).on 'ajax:before', '#f[data-remote]', (e, data, status, xhr) =>
      url = $('[data-url-last-page]').data 'url-last-page'
      @jumpToPage url, false if url != undefined
    $(document).on 'ajax:success', '#f[data-remote]', (e, data, status, xhr) =>
      @clearForm()

  clearForm: ->
    $('#f').each ->
      $(this).find('textarea').val ''
      $(this).find('#remaining').addClass('is-hidden')
      $(this).find('article.message.is-error').remove()
      $(this).find('.is-auto-focused').focus()
      $(this)[0].scrollIntoView()
