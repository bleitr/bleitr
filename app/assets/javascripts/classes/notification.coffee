# Here we show real-time updates to enhance the user experience.
# E.g. new posts.

class @Notification
  constructor: ->
    @caching = false
    @init()

  init: ->
    room = $('span[data-notification-room]').data 'notification-room'
    if room != undefined
      switch room.substring(0, 1)
        when 'h' then @subscribeHome room
        when 'r' then @subscribeRoom room
        when 't' then @subscribeTopic room

  subscribeHome: (room) ->
    App.cable.subscriptions.create { channel: 'ReplyChannel', room: room },
      received: (data) =>
        @doWithCaching(-> $.getScript location.href)

  subscribeRoom: (room) ->
    App.cable.subscriptions.create { channel: 'ReplyChannel', room: room },
      received: (data) =>
        # We only refresh if user is on the FIRST page.
        if $('[data-url-first-page]').size() == 0
          @doWithCaching(-> $.getScript location.href)

  subscribeTopic: (room) ->
    App.cable.subscriptions.create { channel: 'ReplyChannel', room: room },
      received: (data) ->
        # We only append new replies if user is on the LAST page.
        if $('[data-url-last-page]').size() == 0
          if $("div##{data.reply_id}").size() == 0
            $('#replies-new').append($(data.reply).hide().fadeIn())

  # This throttles updates on home and room indexes.
  # When we receive multiple updates in the caching time span, only one
  # will be done, saving some round trips.
  doWithCaching: (f) =>
    if !@caching
      @caching = true
      setTimeout (=>
        f()
        @caching = false
      ), 500
