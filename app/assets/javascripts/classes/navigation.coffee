# These functions hide or show certain elements, or control
# navigation.

class @Navigation
  constructor: ->
    @hideForm()
    @activateMainButton()

  hideForm: ->
    $('#f.is-hidden-by-js').hide()

  showForm: ->
    $('#f').each ->
      $(this).show()
      $(this)[0].scrollIntoView()
      $('.is-auto-focused', this).focus()

  submitForm: ->
    $('#f:visible').each ->
      $('input[type=submit]', this).click()

  navRoot: ->
    window.location.href = window.location.origin ? \
      "#{window.location.origin}/" : \
      "#{window.location.protocol}/#{window.location.host}/"

  navRoom: ->
    $('a.is-room-nav').each ->
      window.location.href = $(this).attr('href')

  activateMainButton: ->
    $('a.button.is-main').on 'click', (e) =>
      e.preventDefault()
      @showForm()
