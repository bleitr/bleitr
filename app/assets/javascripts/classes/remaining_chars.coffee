# When the user has a low amount of chars remaining in the textarea,
# we show how many are remaining, on keyup.

class @RemainingChars
  constructor: ->
    @showRemainingChars()

  showRemainingChars: ->
    max = 1000
    $(document).on 'keyup', 'textarea', (e) ->
      chars_left = max - $(this).val().length
      if chars_left <= max/2
        $('#remaining').removeClass('is-hidden')
        $('#remaining_value').html(chars_left)
      else
        $('#remaining').addClass('is-hidden')
