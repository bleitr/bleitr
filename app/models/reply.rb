class Reply < Message
  belongs_to :topic, counter_cache: true
  paginates_per Rails.configuration.x.replies_per_page
  before_create :set_author_and_tripcode
  before_create :set_options
  before_create :parse_dice
  after_create :bump_date
  after_commit :notify, on: :create
  validate :max_replies

  def fake_offset
    20_000
  end

  def fakeid
    id + fake_offset
  end

  private

  def set_author_and_tripcode
    return self.author = topic.room.default_author if name.blank?

    use_tripcode Tripcode.parse(name, Rails.configuration.x.tripcode_seed)
  end

  def use_tripcode(parsed)
    self.author = parsed[0][0...20].presence || topic.room.default_author
    self.tripcode = parsed[2].presence || parsed[1]
  end

  def parse_dice
    # This assumes we passed validation, so the input is ok and the total
    # message size will not bypass its limits, the total number of dice
    # rolls has not been exceeded and there aren't any invalid rolls.

    # Make sure we skip rolls between code tags.
    self.body = body.gsub(/`[^`]+`/) do |s|
      s.gsub(/#(\d{0,1}d)/, ':~:\\1')
    end
    # Roll dice.
    self.body = body.gsub(/#\d{0,2}d\d{1,3}([+-]\d{1,2})?/) do |s|
      "{d!#{roll_dice s}}"
    end
    # Change back what was between code tags.
    self.body = body.gsub(/:~:/, '#')
  end

  def roll_dice(input)
    # Remove first char and convert d6 to 1d6.
    dice = GamesDice.create input[1..-1].sub(/\Ad/, '1d')

    "#{input}!#{dice.roll}!#{dice.explain_result}"
  end

  def set_options
    return if options.blank?

    self.is_sage = options.include? 'sage'
  end

  def bump_date
    # Make sure to invalidate cache if sage is used.
    return topic.touch if is_sage

    topic.update_attribute(:bumped_at, DateTime.current)
  end

  def max_replies
    return if topic.blank?

    max_replies = topic.room.max_replies
    return if max_replies.zero? || topic.replies.size < max_replies

    errors.add :base, 'Aan deze discussie kunnen geen berichten meer ' \
                      "toegevoegd worden (max. #{max_replies})"
  end

  def notify
    ReplyRelayJob.perform_later(self)
  end
end
