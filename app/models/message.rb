class Message < ApplicationRecord
  # This is an abstract class which both topic and reply classes can inherit.
  #
  # http://api.rubyonrails.org/classes/ActiveRecord/Inheritance/ClassMethods.html
  #
  # "Set this to true if this is an abstract class (see abstract_class?). If you
  # are using inheritance with ActiveRecord and don't want child classes to
  # utilize the implied STI table name of the parent class, this will need to
  # be true."
  self.abstract_class = true

  attr_accessor :name, :options

  validates :name, allow_blank: true, length: { in: 1..100 }
  validates :options, allow_blank: true, length: { in: 1..50 }
  validates :body, presence: true, length: { maximum: 1000 }
  validates :body, format: { without: /\{d\!.+\}/,
                             message: 'bevat ongeldige tekst' }
  validate :dice_validator

  private

  DICE_VALIDATIONS = [
    {
      regexp: /#\d{0,2}d(10[1-9]{1}|1[1-9]{1}\d|[2-9]{1}\d{2}|\d{4,})/,
      message: 'bevat een ongeldige teerlingworp: max zijden is 100'
    },
    {
      regexp: /#(1[1-9]{1}|[2-9]{1}\d|\d{3,})d/,
      message: 'bevat een ongeldige teerlingworp: max teerlingen in één worp ' \
               'is 10'
    },
    {
      regexp: /#\d{0,2}d\d{1,3}[+-]\d{3,}/,
      message: 'bevat een ongeldige teerlingworp: modifiers zijn min. -99 en ' \
               'max. +99'
    }
  ].freeze

  def dice_validator
    return if body.blank?

    # We don't allow more than 5 dice rolls per reply.
    body.scan(/#\d{0,2}d\d{1,3}([+-]\d{1,2})?/m).size > 5 &&
      errors.add(:body, 'bevat teveel teerlingworpen (max: 5)')

    # Run other validations
    DICE_VALIDATIONS.each { |v| validate_body v[:regexp], v[:message] }
  end

  def validate_body(regexp, message)
    regexp.match?(body) && errors.add(:body, message)
  end
end
