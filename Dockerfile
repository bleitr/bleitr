FROM ruby:2.7.0-alpine

ARG BUNDLE_WITHOUT=development

ENV RAILS_ENV=${RAILS_ENV:-production} \
  RAILS_LOG_TO_STDOUT=yes \
  TZ=Europe/Brussels

WORKDIR /usr/src/app
RUN apk add --no-cache --upgrade \
    git \
    libpq \
    nodejs \
    tzdata

COPY Gemfile* ./
RUN apk add --no-cache --upgrade --virtual .builddeps \
    build-base \
    libffi-dev \
    postgresql-dev \
    zlib-dev \
  && bundle install \
  && apk del .builddeps \
  && rm -fr ~/.bundle ~/.gem /usr/local/bundle/cache \
  && rm -fr /usr/bin/np[mx] /usr/lib/node_modules/npm

COPY . .

RUN SECRET_KEY_BASE=assets bin/rails assets:precompile

EXPOSE 3000
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
