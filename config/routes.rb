Rails.application.routes.draw do
  root 'home#index'
  get '/b', to: 'rooms#index', as: 'rooms'
  get '/b/:room_name', to: 'rooms#show', as: 'room'
  post '/b/:room_name', to: 'topics#create'
  get '/b/:room_name/:topic_id', to: 'topics#show', as: 'topic'
  post '/b/:room_name/:topic_id', to: 'replies#create'
  get '/vgv', to: 'static#vgv', as: 'vgv'

  get '/js/preview/:topic_id/:reply_fakeid', to: 'replies#hover', as: 'js_preview'
end
