require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Bleitr
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Set timezone.
    config.time_zone = 'Europe/Brussels'

    # Set default locale.
    config.i18n.default_locale = :nl

    # Let forms work with javascript disabled.
    config.action_view.embed_authenticity_token_in_remote_forms = true

    # Don't log skylight output.
    config.skylight.logger = Logger.new("/dev/null")

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
