# Title.
Rails.application.config.x.title = 'bleitr'

# Show commit version in footer.
Rails.application.config.x.commit = `git rev-parse --short HEAD`.strip

# Replies per page.
Rails.application.config.x.replies_per_page = 100

# Tripcode seed. Set this via env variable or change this.
Rails.application.config.x.tripcode_seed = ENV['TRIPCODE_SEED']

# Don't write out error divs on form errors.
# https://coderwall.com/p/s-zwrg/remove-rails-field_with_errors-wrapper
ActionView::Base.field_error_proc = proc do |html_tag, _instance|
  html_tag.html_safe
end
